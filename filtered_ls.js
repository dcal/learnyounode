var fs = require('fs');

var listFiles = function(path){
  var path = process.argv[2];
  var extName = process.argv[3];

  var directory = fs.readdir(path, function(error, files){
    files.map(function(filename){
      if( findExtension(filename) === extName ){
	console.log(filename);
      }
    });
  });
}

var findExtension = function(filename){
  var fileArray = filename.split(".");
  if( fileArray.length >= 2 ){
    return fileArray.pop();
  }
}

listFiles();
