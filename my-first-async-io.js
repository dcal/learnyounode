var fs = require('fs');

var countLineBreaks = function(callback){
	fs.readFile(process.argv[2], 'utf-8', function doneReading(err, fileString){
		callback(fileString.split("\n").length - 1);
	});
}

var logReturns = function( returns ){
	console.log(returns);
}

countLineBreaks(logReturns);
