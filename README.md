# README #

This repo documents solutions to the learnyounode tutorial available at [http://nodeschool.io](http://nodeschool.io).

For the sake of this tutorial, I stuck primarily to node core modules rather than bringing in third-party modules that may have simplified things a little.

For the sake of experimenting with asynchronous vs synchronous code, I also solved a couple of the problems using ruby, which is why you see a couple of .rb files in here.