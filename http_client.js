var http = require('http');
var url = process.argv[2];

var performRequest = function(url){
	http.get(url, function(res){
		res.setEncoding("utf8");
		res.on("data", function(data){
			console.log(data);
		});
		res.on("error", function(error){
			console.log(error);
		});
	});
}

performRequest(url);
