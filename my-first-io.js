var fs = require('fs');

var countLineBreaks = function(str){
	return (str.split("\n").length - 1);
}

var file = fs.readFileSync(process.argv[2], 'utf-8');
var returns = countLineBreaks(file);

console.log(returns);
