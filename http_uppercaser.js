var http   = require('http'),
	upcase = require('./uppercaser');
    port   = process.argv[2];

var handleRequest = function(request, response){
	var uc = new upcase();
	if (request.method == 'POST') {
		request.pipe(uc).pipe(response);
	}
}
var server = http.createServer( handleRequest ).listen(port);
