var http = require('http');
var url = process.argv[2];
var splitUrl = url.split(":");
var port = splitUrl.pop();
var host = splitUrl[1].split("/").pop();

var options = {
	host: host,
	port: port,
	path: '/'
}

var parseResponse = function(response){
	var responseCollection = [];
	response.setEncoding('utf8');
	response.on('data', function(data){
		responseCollection.push(data);
	});
	response.on('end', function(){
		str = responseCollection.join('');
		len = str.length;
		console.log(len);
		console.log(str);
	});
}

http.request(options, parseResponse)
	.on('error', function(e){
		console.log(e);
	}).end();
