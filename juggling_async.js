var http = require('http');
var count = 0;
var args = process.argv.slice(2,5);

var printToConsole = function(results){
	var keys = Object.keys(results);
	keys.forEach(function(key){
		if(results[key].length == 0){
			console.log('');
		}else{
			console.log(results[key].join(''));
		}
	});
}

var httpSequence = function(urls, callback){
	var container = {};
	urls.forEach(function(url){
		container[url] = [];
		http.get(url, function(response){
			response.setEncoding('utf8');
			response.on('data', function(data){
				container[url].push(data);
			});
			response.on('end', function(){
				count += 1;
				if(container['report'] == 3){
					callback(container);
				}
			});
		});
	});
}

httpSequence(args, printToConsole);
