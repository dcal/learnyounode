var http = require('http'),
    fs   = require('fs'),
    port = process.argv[2],
    file = process.argv[3];

var fileStream = function(filepath, dst){
    var stream = fs.createReadStream(filepath);
    stream.pipe(dst);
}

var server = http.createServer(function(req, res){
    fileStream(file, res);
});

server.listen(port);
