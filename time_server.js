var net = require('net');
var strftime = require('strftime');

var port = process.argv.pop();

var getTime = function(sock){
    sock.end(strftime('%Y-%m-%d %H:%M'));
}

var server = net.createServer( function(socket){
	getTime(socket);
});

server.listen(port);
