var fs = require('fs');

var applyFilter = function(files, extension){
	var filtered = [];
	files.forEach(function(filename){
		arr = filename.split('.');
		if((arr.length > 1) && (arr.pop() === extension)){
			filtered.push(filename);
		}
	});
	return filtered;
}

var filterByExtension = function(filepath, extension, callback){
	fs.readdir(filepath, function(error, files){
		if( error ){
			callback(error)
		}else{
			var filtered = applyFilter(files, extension);
			callback(null, filtered);
		}
	});
}

module.exports = filterByExtension;
