require_relative 'extension_filter'
include FilterByExtension

filepath = ARGV[0]
extension = ARGV[1]

puts "STARTING\n"
filter_by_extension(filepath, extension){ |files|
  begin
    files.each do |d|
      puts d
    end
  rescue StandardError => msg
    puts "An error occurred: #{msg}"
  end
}
puts "FINISHED"
