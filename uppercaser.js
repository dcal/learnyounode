var stream = require('stream'),
    util   = require('util');

var Transform = stream.Transform;

var UpCase = function(options) {
	if (!(this instanceof UpCase)) {
		return new UpCase(options);
	}
	Transform.call(this, options);
}

util.inherits(UpCase, Transform);

UpCase.prototype._transform = function(chunk, encoding, done) {
    var upCased = chunk.toString().toUpperCase();
    this.push(upCased);
    done();
}

module.exports = UpCase;
