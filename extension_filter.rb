module FilterByExtension

  def apply_filter(files, extension)
    filtered = []
    files.each do |filename|
      arr = filename.split(".")
      if (arr.length > 1) && (arr.last == extension)
        filtered << filename.split("/").last
      end
    end
    filtered
  end

  private :apply_filter

  def filter_by_extension(filepath, extension, &block)
    files = Dir["#{filepath}/*"] # this is blocking code
    if files.empty?
      raise "Invalid or empty directory"
    else
      yield apply_filter(files, extension) if block_given?
    end
  end

end
