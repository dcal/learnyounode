function numerify(n) {
	if (!isNaN(parseFloat(n)) && (isFinite(n))){
		return Number(n);
	}else{
		return 0;
	}
}

function babySteps( args ){
	var total = args.reduce(function(a,b){
		return a + b;
	});
	console.log(total);
}

var numbers = process.argv.map(numerify);

babySteps(numbers);
